

# bugly mapping文件需要开启混淆才会生成


- 纯Java代码的工程：只需要配置混淆后生成的`Mapping文件`即可；

- 含有Native代码的工程：建议配置符号表工具从Debug SO中提取的`Symbol符号表`文件。


[Bugly Android SDK 使用指南](https://bugly.qq.com/docs/user-guide/instruction-manual-android/?v=20200622202242)



