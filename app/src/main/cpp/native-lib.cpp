#include <jni.h>
#include <string>
#include <stdlib.h>
#include <stdio.h>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_mynativec_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++ ";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_mynativec_MainActivity_stringToJNI(JNIEnv *env, jobject thiz, jstring str) {
    char *jnistr = (char *) env->GetStringUTFChars(str, NULL);
    strcat(jnistr, ": I am JNI by CMake building");
    return env->NewStringUTF(jnistr);
}